#include <stdio.h>
#include <stdlib.h>

int chargement(int nCarte[],int tPoint[],int tEtat[],int tPhys);
void affichage(int nCarte[],int tPoint[],int tEtat[], int tLog);
void affichageUnique(int tnCarte[], int tPoint[], int tEtat[], int tLog);
int recherche(int tnCarte[], int tLog, int val, char *trouve);
int insertion(int tnCarte[], int tPoint[],int tEtat[], int tBus[], int tTram[], int tMetro[], int tVelo[], int tTrot[],int *tLog,int tphys);
int suppression(int tnCarte[], int tPoint[], int tEtat[], int tBus[], int tTram[], int tMetro[], int tVelo[], int tTrot[], int *tLog, int tPhys);
void afficherBilan(int tBus[], int tTram[], int tMetro[], int tVelo[], int tTrot[], int tLog);
int reserver(int tnCarte[], int tPoint[], int tEtat[], int tBus[], int tTram[], int tMetro[], int tVelo[], int tTrot[], int tLog);
void enregistrementTableaux(int tnCarte[], int tPoint[], int tEtat[], int tLog);
void recharger(int tnCarte[], int tPoint[], int tEtat[], int tLog, int carte);
int calculBonus(int ajout);
void gererEtatCarte(int tnCarte[], int tEtat[], int tLog);
void globale(void);