#include "SAE.h"

int chargement(int tnCarte[],int tPoint[],int tEtat[],int tPhys)
{
	int i = 0,nCarte,point,etat;/*déclaration d'un compteur(i) et de variables intermédiaires pour l'assignation des tableaux*/
	FILE *flot;/*formalités pour les fichiers*/
	flot=fopen("clients.txt","r");
	if (flot==NULL)
	{
		printf("Problème d'ouverture du fichier clients.txt\n");
		return -1;
	}
	fscanf(flot,"%d%d%d",&nCarte,&point,&etat);
	while(!feof(flot))/*remplissage des tableaux*/ 
	{
		if (i > tPhys)
		{
			printf("Tableaux plein.\n");/*tableaux pleins donc on retourne -1*/
			fclose(flot);
			return -1;
		}
		tnCarte[i] = nCarte;
		tPoint[i] = point + 2; /*ajoute 2 points bonus à chaque usager pendant le chargement*/
		tEtat[i] = etat;
		fscanf(flot,"%d%d%d",&nCarte,&point,&etat);
		i++;
	}
	fclose(flot); 
	return i; /*retourne si le chargement s'est bien passé la taille logique des tableaux(i)*/
}


void affichage(int tnCarte[], int tPoint[], int tEtat[],int tLog) /*affiche les informations de tous les usagers enregistrés*/
{
	int i = 0;
   	printf("|----------------------------------------------|\n"); /*affichage un peu plus esthétique*/
	printf("|   N°Carte   |   Nb Points    |   Etat Carte  |\n");
	printf("|----------------------------------------------|\n");
	while(i < tLog)
	{
		if(tEtat[i] == 1)
		{
			printf("|    %5d    |\t     %3d       |    Bloquée    |\n",tnCarte[i],tPoint[i]);
			printf("|----------------------------------------------|\n");
		}
		else
		{
			printf("|    %5d    |\t     %3d       |   Débloquée   |\n",tnCarte[i],tPoint[i]);
			printf("|----------------------------------------------|\n");
		}
		i ++;
	}
}


void affichageUnique(int tnCarte[], int tPoint[], int tEtat[], int tLog) /*affiche les informations d'une seule personne*/
{
	int pos, numeroC;
	char trouve;
	printf("Quel est le numéro de carte de l'usager ?\nSaisie : ");
	scanf("%d%*c", &numeroC);
	printf("\n");
	pos = recherche(tnCarte, tLog, numeroC, &trouve);
	if(trouve == 'N')
	{
		printf("Erreur, l'usager n'existe pas\n");
		return;
	}
	printf("|----------------------------------------------|\n"); /*affichage un peu plus esthétique*/
	printf("|   N°Carte   |   Nb Points    |   Etat Carte  |\n");
	printf("|----------------------------------------------|\n");
	if(tEtat[pos] == 1)
		{
			printf("|    %5d    |\t     %3d       |    Bloquée    |\n",tnCarte[pos],tPoint[pos]);
			printf("|----------------------------------------------|\n");
		}
		else
		{
			printf("|    %5d    |\t     %3d       |   Débloquée   |\n",tnCarte[pos],tPoint[pos]);
			printf("|----------------------------------------------|\n");
		}
}

	
int recherche(int tnCarte[], int tLog, int val, char *trouve) /* recherche le numéro de carte dans le tableaux */
{
	int i = 0;
	if(val == tnCarte[0])
	{
		*trouve = 'O';
		return i;
	}
	while(val > tnCarte[i])
	{
		if(i == tLog)
		{
			return -1;
		}
		i++;
	}
	if(val == tnCarte[i])
	{
		*trouve = 'O';
	}
	else
	{
		*trouve = 'N';
	}
	return i;
}



int insertion(int tnCarte[], int tPoint[],int tEtat[], int tBus[], int tTram[], int tMetro[], int tVelo[], int tTrot[],int *tLog,int tphys)
{
	int i,j,numeroC,points = 0, taille;
	char trouve;
	printf("Quel est le numéro de carte à insérer ?\nSaisie : ");
	scanf("%d%*c", &numeroC);
	i = recherche(tnCarte,*tLog,numeroC,&trouve);
	if (trouve == 'O') /* Si la carte existe déjà, la fonction renvoie -1 */
	   {
	    printf("\nLe numéro de carte existe déjà\n");
	    return -1;
	   }
	if (*tLog == tphys)
	   {
	    printf("\nErreur, les tableaux sont pleins\n");
	    return -1;
		}
	if (trouve == 'N')
	   {
	   for (j = *tLog - 1; j >= i; j--) /* Décalage vers la doroite des valeurs des tableaux */
	   	{
	     	 tnCarte[j+1] = tnCarte[j];
			 tPoint[j+1] = tPoint[j];
			 tEtat[j+1] = tEtat[j];
			 tBus[j+1] = tBus[j];
			 tTram[j+1] = tTram[j];
			 tMetro[j+1] = tMetro[j];
			 tVelo[j+1] = tVelo[j];
			 tTrot[j+1] = tTrot[j];
			}
	   }
		 tnCarte[i] = numeroC;
		 tPoint[i] = points; /* Par défaut, la carte doit être créditée*/
		 tEtat[i] = 0; /* Par défaut, la carte est débloquée*/
		 tBus[i] = 0; /*Par défaut, aucun transport n'est réservé */
		 tTram[i] = 0;
		 tMetro[i] = 0;
		 tVelo[i] = 0;
		 tTrot[i] = 0;
	*tLog = *tLog + 1;
	taille = *tLog;
	recharger(tnCarte,tPoint,tEtat,taille,numeroC);
	system("clear");
	return 0;
}


int suppression(int tnCarte[], int tPoint[], int tEtat[], int tBus[], int tTram[], int tMetro[], int tVelo[], int tTrot[], int *tLog, int tPhys)
{
	int i, numeroC;
	char trouve, choix;
	printf("Quel est le numéro de la carte à supprimer ?\n");
	scanf("%d%*c", &numeroC);
	printf("Êtes-vous sûr de vouloir supprimer la carte n°%d ? (O/N)\nSaisie : ",numeroC);
	scanf("%c%*c", &choix);
	printf("\n");
	if(choix == 'O')
	{
		i = recherche(tnCarte, *tLog, numeroC, &trouve);
		if(trouve == 'N')
		{
			printf("Erreur, le numéro de carte saisie n'existe pas\n");
			return -1;
		}
		while(i <= *tLog) /* Décalage vers la gauche des valeurs dans les tableaux*/
			{
				tnCarte[i] = tnCarte[i+1];
				tPoint[i] = tPoint[i+1];
				tEtat[i] = tEtat[i+1];
				tBus[i] = tBus[i+1];
				tTram[i] = tTram[i+1];
				tMetro[i] = tMetro[i+1];
				tVelo[i] = tVelo[i+1];
				tTrot[i] = tTrot[i+1];
				i++;
			}
		*tLog = *tLog -1;
	}
	system("clear");
}


void afficherBilan(int tBus[], int tTram[], int tMetro[], int tVelo[], int tTrot[],int tLog)
{
	int i = 0, nbBus = 0, nbTram = 0, nbMetro = 0, nbVelo = 0, nbTrot = 0;
	while(i < tLog) /* Calcule la somme de tous les éléments ppur chaque tableau de transport, soit le nombre d'usagers de la journée */
		{
			nbBus = nbBus + tBus[i];
			nbTram = nbTram + tTram[i];
			nbMetro = nbMetro + tMetro[i];
			nbVelo = nbVelo + tVelo[i];
			nbTrot = nbTrot + tTrot[i];
			i++;
		}
	printf("Bus\tTram\tMétro\tVelo\tTrottinette\n");
	printf("%d\t%d\t%d\t%d\t%d\n", nbBus, nbTram, nbMetro, nbVelo, nbTrot); /* Affiche le bilan de la journée */
	}


int reserver(int tnCarte[], int tPoint[], int tEtat[], int tBus[], int tTram[], int tMetro[], int tVelo[], int tTrot[], int tLog)
{
	int pos, numeroC,saisie;
	char trouve;
	system("clear");
	printf("Quel est le numéro de la carte à débiter ?\nSaisie : ");
	scanf("%d%*c", &numeroC);
	printf("\n");
	pos = recherche(tnCarte, tLog, numeroC, &trouve);
	if(trouve == 'N') /* Si la carte n'est pas trouvée, la fonction retourne -1 */
		{
		printf("Erreur, le numéro de catrte n'existe pas");
		return -1;
		}
	if(tEtat[pos] == 1) /* Si la carte est bloquée, la fonction retourne -1 */
		{
		printf("Erreur, cette carte est bloquée\n");
		return -1;
		}
	printf("Quel moyen de transport souhaitez-vous réserver ?\n\n");
	printf("|-------------------------------|\n");
	printf("| 1 | Bus(4 points)\t\t|\n| 2 | Tram(4 points)\t\t|\n| 3 | Metro(6 points)\t\t|\n| 4 | Velo(3 points)\t\t|\n| 5 | Trottinette(2 points)     |\n| 6 | Tout réserver(15 points)  |\n| 7 | Quitter\t\t\t|\n");
	printf("|-------------------------------|\n\n");
	printf("Il vous reste %d points sur votre carte.\n\nSaisie : ", tPoint[pos]);
	scanf("%d%*c", &saisie);
	system("clear");
	while(saisie != 7) /* Gestion du menu des types de transports */
	 {
		if(saisie == 1)
			{
			if(tBus[pos] == 1) /* Impossible de réserver un type de transport plusieurs fois dans une journée */
				{
				printf("Le bus est déjà réservé pour cet usager\n\n");
				}
			else
				{
				if(tPoint[pos] < 4)
					{
						printf("Vous ne possédez pas assez de points.\n\n");
					}
				else
					{
						tPoint[pos] = tPoint[pos] - 4; /* Débitement des points sur la carte */
						tBus[pos] = 1; /* Réservation du type de transport */
					}
				}
			}
		if(saisie == 2)
			{
			if(tTram[pos] == 1)
				{
				printf("Le tram est déjà réservé pour cet usager\n\n");
				}
			else
				{
					if(tPoint[pos] < 4)
						{
							printf("Vous ne possédez pas assez de points.\n\n");
						}
					else
						{
							tPoint[pos] = tPoint[pos] - 4;
							tTram[pos] = 1;
						}
				}
			}
		if(saisie == 3)
			{
			if(tMetro[pos] == 1)
				{
				printf("Le métro est déjà réservé pour cet usager\n\n");
				}
			else
				{
				if(tPoint[pos] < 6)
					{
						printf("Vous ne possédez pas assez de points.\n\n");
					}
				else
					{
						tPoint[pos] = tPoint[pos] - 6;
						tMetro[pos] = 1;
					}
				}
			}
		if(saisie == 4)
			{
			if(tVelo[pos] == 1)
				{
				printf("Le vélo est déjà réservé pour cet usager\n\n");
				}
			else
				{
				if(tPoint[pos] < 3)
					{
						printf("Vous ne possédez pas assez de points.\n\n");
					}
				else
					{
						tPoint[pos] = tPoint[pos] - 3;
						tVelo[pos] = 1;
					}
				}
			}
		if(saisie == 5)
			{
			if(tTrot[pos] == 1)
				{
				printf("La trottinette est déjà réservée pour cet usager\n\n");
				}
			else
				{
				if(tPoint[pos] < 2)
					{
						printf("Vous ne possédez pas assez de points.\n\n");
					}
				else
					{
						tPoint[pos] = tPoint[pos] - 2;
						tTrot[pos] = 1;
					}
				}
			}
		if(saisie == 6)
			{
				if(tPoint[pos] < 15)
					{
						printf("Vous ne possédez pas assez de points.\n\n");
					}
				else
					{
						tPoint[pos] = tPoint[pos] - 15;
						tBus[pos] = 1;
						tTram[pos] = 1;
						tMetro[pos] = 1;
						tVelo[pos] = 1;
						tTrot[pos] = 1;
					}
			}
		if(saisie == 7)
			{
				break;
			}
		printf("Quel moyen de transport souhaitez-vous réserver ?\n\n");
		printf("|-------------------------------|\n");
		printf("| 1 | Bus(4 points)\t\t|\n| 2 | Tram(4 points)\t\t|\n| 3 | Metro(6 points)\t\t|\n| 4 | Velo(3 points)\t\t|\n| 5 | Trottinette(2 points)     |\n| 6 | Tout réserver(15 points)  |\n| 7 | Quitter\t\t\t|\n");
		printf("|-------------------------------|\n\n");
		printf("Il vous reste %d points sur votre carte.\n\nSaisir : ", tPoint[pos]);
		scanf("%d%*c", &saisie);
		system("clear");
	 }
}


void enregistrementTableaux(int tnCarte[], int tPoint[], int tEtat[], int tLog)
{
    int  i = 0;
	FILE *flot;
    flot = fopen("clients.txt","w");
    if (flot==NULL)
    {
        printf("Erreur d'ouverture du fichier clients.txt");
        return;
    }
    while (i < tLog)
    {
        fprintf(flot,"%d\t%d\t%d\n",tnCarte[i],tPoint[i],tEtat[i]); //jsp si c'est mieux de passer par des valeurs intermédiaires  pour l'écriture
        i++;
    }
}

int calculBonus(int ajout) //en travaux / calcul l'ajout de point à une carte / est appelée dans la fonction recharger et dans insertion 
{
	int bonus;
	if (ajout>10)
	{
		if(ajout>15)
		{
			if (ajout>20)
			{
				if (ajout>30)
				{
					if (ajout>45)
					{
						if (ajout>75)
						{
							if (ajout>100)
								bonus=20;
							else bonus=15;
						}
						else bonus=10;
					}
					else bonus=8;
				}
				else bonus=5;
			}
			else bonus=3;
		}
		else bonus=2;
	}
	else bonus=0;
	return bonus;
}

void recharger(int tnCarte[], int tPoint[], int tEtat[], int tLog, int carte)
{
    char trouve, choix;
    int index, ajout, bonus;
    float cout;
    if(carte == 0)
    {
		printf("Quelle carte voulez-vous recharger ?\nSaisie : ");
    	scanf("%d%*c",&carte);
    }
    index = recherche(tnCarte,tLog,carte, &trouve);     //on regarde si la carte existe et on recupere son index
    while (trouve != 'O')                              //on demande une carte valide tant que la carte n'est pas trouvée
    {
        printf("\nCette carte n'existe pas, re-saisir : ");
        scanf("%d%*c",&carte);
        index = recherche(tnCarte,tLog,carte,&trouve);
    }
    if(tEtat[index] == 1)
    {
    	printf("\nCette carte est bloquée, vous ne pouvez pas la recharger.\n");
    	return;
    }
    printf("\nIl y a actuellement %d points sur cette carte\nCombien de points voulez vous ajouter ?\nSaisie : ",tPoint[index]);
    scanf("%d%*c",&ajout);
    cout = ajout * 1.50;/*prix à voir + est ce qu'on ferait pas des reduc par paliers de points*/;
    bonus = calculBonus(ajout);
    printf("\nCela vous fera %.2f€\n",cout);
    /*PAIEMENT*/
    printf("Souhaitez-vous continuer ? (O/N)\nSaisie : ");
    scanf("%c%*c",&choix);
    printf("\n");
    printf("\n");
    if(choix == 'N')
    {
    	printf("Rechargement annulé\n");
    	return;
    }
    else
    {
    	tPoint[index] = tPoint[index] + ajout + bonus;
    }
    system("clear");
}

void gererEtatCarte(int tnCarte[], int tEtat[], int tLog)
{
	int numeroC, pos;
	char trouve, saisie;
	printf("Quelle carte souhaitez-vous gérer ?\nSaisie : ");
	scanf("%d%*c", &numeroC);
	pos = recherche(tnCarte, tLog, numeroC, &trouve);
	if(tEtat[pos] == 1)
	{
		printf("Cette carte est actuellement bloquée.\n");
		printf("\nSouhaitez-vous la débloquer ? (O/N)\nSaisie : ");
		scanf("%c%*c", &saisie);
		if (saisie == 'O')
		{
			tEtat[pos] = 0;
			printf("\nVotre carte est désormais débloquée.\n");
		}
	}
	else
	{
		printf("Cette carte est actuellement débloquée.\n");
		printf("Souhaitez-vous la bloquer ? (O/N)\nSaisie : ");
		scanf("%c%*c", &saisie);
		if (saisie == 'O')
		{
			tEtat[pos] = 1;
			printf("\nVotre carte est désormais bloquée.\n");
		}
	}
}


void globale(void)
{
	int tnCarte[100] = {0}, tPoint[100] = {0}, tEtat[100] = {0},tPhys = 100,tBus[100] = {0},tTram[100] = {0},tMetro[100] = {0},tVelo[100] = {0},tTrot[100] = {0}, tLog, choix, carte = 0;
	tLog = chargement(tnCarte, tPoint, tEtat, tPhys); /* Chargement du fichier dans les tableaux */
	if(tLog == -1)
		return;
	system("clear");
	printf("\n------------------------------------------\n");
	printf("| \tGestion des transports\t\t |\n");
	printf("------------------------------------------\n");
	printf("|   1  Réserver un type de transport\t |\n");
	printf("|   2  Afficher tous les usagers\t |\n");
	printf("|   3  Afficher un usager\t\t |\n");
	printf("|   4  Recharger une carte\t\t |\n");
	printf("|   5  Insérer un nouvel usager\t\t |\n");
	printf("|   6  Supprimer un usager\t\t |\n");
	printf("|   7  Afficher le bilan de la journée\t |\n");
	printf("|   8  Bloquer/Débloquer une carte\t |\n");
	printf("|   9  Quitter\t\t\t\t |\n");
	printf("------------------------------------------\n\n");
	printf("Saisie : ");
	scanf("%d%*c",&choix);
	printf("\n");
	while(choix != 9) /* Gestion du menu général du programme */
	{
		if(choix == 1)
			reserver(tnCarte,tPoint,tEtat,tBus,tTram,tMetro,tVelo,tTrot,tLog);
		if(choix == 2)
			affichage(tnCarte,tPoint,tEtat,tLog);
		if(choix == 3)
			affichageUnique(tnCarte, tPoint, tEtat, tLog);
		if(choix == 4)
			recharger(tnCarte, tPoint, tEtat, tLog, carte);
		if(choix == 5)
			insertion(tnCarte, tPoint, tEtat, tBus, tTram, tMetro, tVelo, tTrot, &tLog, tPhys);
		if(choix == 6)
			suppression(tnCarte, tPoint, tEtat, tBus, tTram, tMetro, tVelo, tTrot, &tLog, tPhys);
		if(choix == 7)
			afficherBilan(tBus, tTram, tMetro, tVelo, tTrot, tLog);
		if(choix == 8)
			gererEtatCarte(tnCarte, tEtat, tLog);
		printf("\n------------------------------------------\n");
		printf("| \tGestion des transports\t\t |\n");
		printf("------------------------------------------\n");
		printf("|   1  Réserver un type de transport\t |\n");
		printf("|   2  Afficher tous les usagers\t |\n");
		printf("|   3  Afficher un usager\t\t |\n");
		printf("|   4  Recharger une carte\t\t |\n");
		printf("|   5  Insérer un nouvel usager\t\t |\n");
		printf("|   6  Supprimer un usager\t\t |\n");
		printf("|   7  Afficher le bilan de la journée\t |\n");
		printf("|   8  Bloquer/Débloquer une carte\t |\n");
		printf("|   9  Quitter\t\t\t\t |\n");
		printf("------------------------------------------\n\n");
		printf("Saisie : ");
		scanf("%d%*c",&choix);
		system("clear");
	}
	enregistrementTableaux(tnCarte,tPoint,tEtat,tLog); /* Ecriture des tableaux dans le fichier */
	system("clear");
}